**Dev On Docker**


## Install Composer Dependencies (Mac)
```sh
docker run --rm -v $(pwd):/app composer install
```
## Or Install Composer Dependencies (Window)
```sh
docker run --rm -v %cd%:/app composer install
```

## Run Docker
```sh
docker-compose up -d
```
if console error about Invalid argument carbon
should remove folder vendor/bin

## Init .env (should update .env eg. user/pass mysql, ip-address match in docker)
```sh
docker-compose exec app cp .env.example .env
```

## Init key for .env 
```sh
docker-compose exec app php artisan key:generate
```

## >>>>>>>>>>Run update config EVERY TIME!! when update .env file
```sh
docker-compose exec app php artisan config:cache
```

## Run Migrate
```sh
docker-compose exec app php artisan migrate
```

## Install Laravel Mix && Build Mix
```sh
docker-compose exec app npm install && npm run dev
```

## Run On
```sh
http://localhost:8000/
```
if error log permission denied
```sh
docker-compose exec app chmod -R 777 storage
```

## Develop !!!!!!
