<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HelloController extends Controller
{
    /**
     *
     * @return \Illuminate\View\View
     */
    public function hello()
    {
        $header = "Learning Laravel";
        return view('hello.index')->with('title', 'Say Hello Laravel')->withHeader($header);
    }
    public function page($id = 0)
    {
        return "Hello: " . $id;
    }
}
