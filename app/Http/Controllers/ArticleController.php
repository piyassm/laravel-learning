<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

// use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::published()->paginate(2);
        $articles2 = Article::unpublished()->take(10)->get();
        $articles3 = Article::published()->take(2)->orderBy('id', 'desc')->get();
        $articles4 = Article::unpublished()->take(2)->orderBy('id', 'desc')->orderBy('published_at', 'desc')->get();
        $articles5 = Article::published()->where([['title', 'LIKE', '%t%'], ['title', 'LIKE', '%e%']])->take(2)->orderBy('id', 'desc')->orderBy('published_at', 'desc')->get();
        //
        return view('articles.index', compact('articles', 'articles2'))->with(compact('articles3', 'articles4', 'articles5'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article();
        $article->title = $request->title;
        $article->body = $request->body;
        $article->published_at = $request->published_at;
        $article->user_id = Auth::user()->id;
        $article->save();
        return redirect('articles');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        if (empty($article))
            abort(404);
        return view('articles.show', compact('article'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        if (empty($article))
            abort(404);
        return view('articles.edit', compact('article'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = Article::find($id);
        if (empty($article))
            abort(404);
        $article->update($request->all());
        return redirect('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        if (empty($article))
            abort(404);
        $article->delete();
        return redirect('articles');
        //
    }
}
