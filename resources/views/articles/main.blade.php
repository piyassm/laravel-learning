@section('nav-menu')
<div>
    <a href="{{ url('/articles/create') }}" class="text-sm text-gray-700 underline">Article Create</a>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/article-style.css') }}" />
@stop