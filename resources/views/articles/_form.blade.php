@include('articles.main')

@if($errors->any())
<ul class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif

@php
$date = date("Y-m-d");
if(!empty($article->published_at)){
    $date = $article->published_at;
}
@endphp

@csrf

<div>
    {!! Form::label('title', 'Title: ') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
</div>
<div>
    {!! Form::label('body', 'Body: ') !!}
    {!! Form::textarea('body', null, ['class'=>'form-control']) !!}
</div>
<div>
    {!! Form::label('published_at', 'Published on: ') !!}
    {!! Form::input('date', 'published_at', \Carbon\Carbon::parse($date)->format('Y-m-d'), ['class'=>'form-control']) !!}
</div>
<div>
    {!! Form::button($submitButtonText, ['class'=>'btn btn-primary form-control', 'type'=>'submit']) !!}
</div>